# calcuate the precision, recall and F1-score



import os
from os.path import abspath, dirname, sys

from utils import BASE_PATH, get_rec_by_user_id, get_rec_by_movie_id

DEBUG = True
threshold = 0
user_movie_ratings = {}
movie_ids = []
user_ids = list(range(1, 6040 + 1))

with open(os.path.join(BASE_PATH, "ml-1m/ratings.dat"), 'r', encoding="ISO-8859-1") as f:
    for i, line in enumerate(f):
        line = line.strip('\r\n')
        user_id, movie_id, rating, _ = line.split('::')
        user_id, movie_id, rating = int(user_id), int(movie_id), float(rating)
        if float(rating) >= threshold:
            if user_id in user_movie_ratings:
                user_movie_ratings[user_id][movie_id] = rating
            else:
                user_movie_ratings[user_id] = {movie_id: rating}

with open(os.path.join(BASE_PATH, "ml-1m/movies.dat"), 'r', encoding="ISO-8859-1") as f:
    for i, line in enumerate(f):
        line = line.strip('\r\n')
        movie_id, _ = line.split('::')[:2]
        movie_ids.append(int(movie_id))

if DEBUG:
	user_ids = user_ids[1000:1010]

# Precision@k = (# of recommended items @k that are relevant) / (# of recommended items @k)
p_a, p_b = 0, 0
for user_id in user_ids:
	items = get_rec_by_user_id(user_id, without_title=0)
	v = 0
	for movie_id, _ in items:
		movie_id = int(movie_id)
		if movie_id in user_movie_ratings[user_id] and user_movie_ratings[user_id][movie_id] >= threshold:
			v += 1
	p_a += v
	p_b += len(items)
	print(v, min(len(user_movie_ratings[user_id]), len(items)))
print("by user, with title, precision: ", p_a / (p_b + 0.0))

p_a, p_b = 0, 0
for user_id in user_ids:
	items = get_rec_by_user_id(user_id, without_title=1)
	v = 0
	for movie_id, _ in items:
		movie_id = int(movie_id)
		if movie_id in user_movie_ratings[user_id] and user_movie_ratings[user_id][movie_id] >= threshold:
			v += 1
	p_a += v
	p_b += len(items)
print("by user, without title, precision: ", p_a / (p_b + 0.0))

# Recall@k = (# of recommended items @k that are relevant) / (total # of relevant items)
p_a, p_b = 0, 0
for user_id in user_ids:
	items = get_rec_by_movie_id(user_id, without_title=0)
	v = 0
	for movie_id, _ in items:
		movie_id = int(movie_id)
		if movie_id in user_movie_ratings[user_id] and user_movie_ratings[user_id][movie_id] >= threshold:
			v += 1
	p_a += v
	p_b += len(user_movie_ratings[user_id])
print("by movie, with title, recall: ", p_a / (p_b + 0.0))

p_a, p_b = 0, 0
for user_id in user_ids:
	items = get_rec_by_movie_id(user_id, without_title=1)
	v = 0
	for movie_id, _ in items:
		movie_id = int(movie_id)
		if movie_id in user_movie_ratings[user_id] and user_movie_ratings[user_id][movie_id] >= threshold:
			v += 1
	p_a += v
	p_b += len(user_movie_ratings[user_id])
print("by movie, without title, recall: ", p_a / (p_b + 0.0))