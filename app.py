#!/usr/bin/env python
import flask
from flask import request
import json
import os

from flask_cors import CORS, cross_origin
from utils import BASE_PATH
from utils import get_rec_by_movie_id, get_rec_by_user_id

app = flask.Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/get_recommendations_user_id', methods=['GET', 'POST'])
@cross_origin()
def get_recommendations():
    params = request.form
    try:
        rec = get_rec_by_user_id(int(params.get("user_id")), int(params.get("without_title", 0)))
        return json.dumps(rec)
    except Exception as e:
        return "error: %s" % e


@app.route('/get_recommendations_by_movie_id', methods=['GET', 'POST'])
@cross_origin()
def get_movies():
    params = request.form
    try:
        rec = get_rec_by_movie_id(params.get("movie_id"), int(params.get("without_title", 0)))  
        return json.dumps(rec)
    except Exception as e:
        return "error: %s" % e


if __name__ == '__main__':
    app.run(debug=True, port=5000)
