#!/usr/bin/env python
# coding: utf-8

# # Recommender System for Group 15
# This project implemented via CNN neural network is the final project for Group 15 in the Web Semantic Mining, Spring 2020 

# In[1]:

# suffix="_without_title" ;
# without_title=True
suffix="" ;
without_title=False 
# In[22]:


import pandas as pd
import numpy as np
import numpy as np
import pandas as pd
import pdb
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable
from sklearn.model_selection import KFold
import torch.backends.cudnn as cudnn
import math


# # MovieLens
# ## users.dat
# User information is in the file "users.dat" and is in the following
# format: **UserID::Gender::Age::Occupation::Zip-code**
# ## movies.dat
# Movie information is in the file "movies.dat" and is in the following
# format: **MovieID::Title::Genres**
# ## ratings.dat
# All ratings are contained in the file "ratings.dat" and are in the
# following format: **UserID::MovieID::Rating::Timestamp**

# In[23]:

# load dataset
with open("./processed/train_feature1.txt", "r") as file:
    train_feature = eval(file.readline())
with open("./processed/train_target1.txt", "r") as file:
    train_target = eval(file.readline())
with open("./processed/test_feature1.txt", "r") as file:
    test_feature = eval(file.readline())
with open("./processed/test_target1.txt", "r") as file:
    test_target = eval(file.readline())
# tensor
train_feature = torch.FloatTensor(train_feature)
train_target = torch.FloatTensor(train_target)
test_feature = torch.FloatTensor(test_feature)
test_target = torch.FloatTensor(test_target)

# tried one-hot label
# train_label=train_target.type(torch.LongTensor)
# train_one_hot = torch.zeros(train_label.shape[0],6).scatter_(1, train_label, 1)
# train_target = train_one_hot.type(torch.FloatTensor)

# test_label=test_target.type(torch.LongTensor)
# test_one_hot = torch.zeros(test_label.shape[0],6).scatter_(1, test_label, 1)
# test_target = test_one_hot.type(torch.FloatTensor)
# pdb.set_trace()
feature_size=train_feature.shape[1]

# fully connect network
class naiveFC(torch.nn.Module):
    def __init__(self, ):
        super(naiveFC, self).__init__()
        self.fc1 = nn.Linear(feature_size,64)
        self.fc2 = nn.Linear(64, 128)
        self.fc3 = nn.Linear(128,200)
        self.fc4 = nn.Linear(200,1)
    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        return x
naiveFC = naiveFC()
device = 'cuda' if torch.cuda.is_available() else 'cpu'
if device =='cuda':
    cudnn.benchmark = True
naiveFC = naiveFC.to(device)

criterion = torch.nn.MSELoss()
optimizer = optim.RMSprop(naiveFC.parameters(), lr = 0.001)
naiveFC.train()

train_number = train_feature.shape[0]
batch_size = 100
batch_number = math.floor(train_number/batch_size)

nb_epoch = 100
for epoch in range(1, nb_epoch + 1):
    train_loss = 0
    # shuffle the data
    index=torch.randperm(train_number)
    train_feature=train_feature[index]
    for index in range(batch_number):
        input = train_feature[index*batch_size:(index+1)*batch_size].to(device)
        target = train_target[index*batch_size:(index+1)*batch_size].to(device)
        optimizer.zero_grad()
        output = naiveFC(input)
        target.require_grad = False
        loss = criterion(output, target)
        loss.backward()
        train_loss += loss.item()
        optimizer.step()
    print('epoch: '+str(epoch)+' loss: '+str(train_loss/train_number))

# test
test_number = test_feature.shape[0]
batch_number = math.floor(test_number/batch_size)
naiveFC.eval()
test_total=0
test_correct=0
for index in range(batch_number):
    input = test_feature[index*batch_size:(index+1)*batch_size].to(device)
    target = test_target[index*batch_size:(index+1)*batch_size].to(device)
    output = naiveFC(input)
    pred=torch.round(output)
    correct=torch.sum(pred==target).item()
    test_correct+=correct
    test_total+=batch_size

print('test ratings accuracy: '+str(test_correct/test_total))


# the attributes for each data-set
users_title=['UserID', 'Gender', 'Age', 'OccupationID', 'Zip-code']
movies_title=['MovieID', 'Title', 'Genres']
ratings_title=['UserID','MovieID', 'Rating', 'Timestamps']


# In[24]:

users=pd.read_table('./ml-1m/users.dat', sep='::', header=None, names=users_title)
# Gender: F => 0, M => 1
# Age: do nothing
# OccupationID: do nothing
users['Gender']=users['Gender'].map({"F": 0, "M": 1})
users=users.drop(['Zip-code'], axis=1)
print(users.values.shape)
users.head()


# In[25]:


movies=pd.read_table('./ml-1m/movies.dat', sep='::', header=None, names=movies_title)

# Convert Genres
# get all the genres
# at most 18 genres
genres_set=set()
for k in movies['Genres'].str.split('|'):
    genres_set.update(k)
    
geners_set_mapping={k:j for j, k in enumerate(genres_set)}
geners_mapping={}
for k in movies['Genres']:
    v=[]
    for i in k.split("|"):
        v.append(geners_set_mapping[i])
    v=v[:18]
    if len(v) < 18:
        v += (18 - len(v)) * [18]
    geners_mapping[k]=v
movies['Genres']=movies['Genres'].map(geners_mapping)

sentences_size=15
# at most 15 words
title_set=set()
strip_title=lambda k: k.replace("'", "").replace("(", " ").replace(")", "")
for k in movies['Title']:
    k=strip_title(k)
    title_set.update(k.split())

title_set_mapping={strip_title(k):j for j, k in enumerate(title_set)}
title_mapping={}
for k in movies['Title']:
    v=[]
    k_ori=k
    # remove special character
    k=strip_title(k)
    for i in k.split():
        v.append(title_set_mapping[i])
    v=v[:15]
    if len(v) < 15:
        v += (15 - len(v)) * [15]
    
    if without_title:  # make it nosense
        v=[15] * 15
    title_mapping[k_ori]=v
if without_title:
    print("create the model without movie title")
movies['Title']=movies['Title'].map(title_mapping)
print(movies.values.shape)
movies.tail()


# In[26]:


ratings=pd.read_table('./ml-1m/ratings.dat', sep='::', header=None, names=ratings_title)
print(ratings.values.shape)
ratings.head()


# In[27]:


data=pd.merge(pd.merge(ratings, users), movies)    
features=data.drop(['Rating', 'Timestamps'], axis=1).values
labels=data['Rating'].values
features.shape, labels.shape


# In[28]:


# the head of the features
data.drop(['Rating', 'Timestamps'], axis=1).head()


# # Build the graph

# In[29]:


import tensorflow as tf


# In[30]:


# global variable for graph
config={
    "user_cnt": max(features.take(0, 1)) + 1, 
    "movie_cnt": max(features.take(1, 1)) + 1,
    "gender_cnt": max(features.take(2, 1)) + 1, # female or male
    "age_cnt": max(features.take(3, 1)) + 1 ,
    "occupation_cnt": max(features.take(4, 1)) + 1,
    "embed_dim": 32, # or try out 64
    "movie_genres_cnt": 19,
    "movie_title_cnt": len(title_set),
}



# In[31]:
tf.reset_default_graph()
train_graph=tf.Graph()

# get the default placeholder for the graph
with train_graph.as_default():
    
    uid, user_gender, user_age, user_occupation=tf.placeholder(tf.int32, [None, 1], name="uid"), tf.placeholder(tf.int32, [None, 1], name="user_gender"), tf.placeholder(tf.int32, [None, 1], name="user_age"), tf.placeholder(tf.int32, [None, 1], name="user_occupation") ;
    movie_id, movie_genres, movie_titles=tf.placeholder(tf.int32, [None, 1], name="movie_id"), tf.placeholder(tf.int32, [None, 18], name="movie_genres"), tf.placeholder(tf.int32, [None, 15], name="movie_titles")

    targets=tf.placeholder(tf.int32, [None, 1], name="targets")
    lr=tf.placeholder(tf.float32, name="lr")

    # create several layers
    with tf.name_scope("user_embedding"):
        gender_em_layer=tf.Variable(tf.random_uniform([config['gender_cnt'], config['embed_dim'] // 2], -1, 1), name= "gender_em_layer")
        gender_elayer=tf.nn.embedding_lookup(gender_em_layer, user_gender, name="gender_elayer")

        age_em_layer=tf.Variable(tf.random_uniform([config['age_cnt'], config['embed_dim'] // 2], -1, 1), name="age_em_layer")
        age_elayer=tf.nn.embedding_lookup(age_em_layer, user_age, name="age_elayer")

        uid_em_layer=tf.Variable(tf.random_uniform([config['user_cnt'], config['embed_dim']], -1, 1), name="uid_em_layer")
        uid_elayer=tf.nn.embedding_lookup(uid_em_layer, uid, name="uid_elayer")

        occupationembedding_layer=tf.Variable(tf.random_uniform([config['occupation_cnt'], config['embed_dim'] // 2], -1, 1), name="occupationembedding_layer")
        occupationembed_layer=tf.nn.embedding_lookup(occupationembedding_layer, user_occupation, name="occupationembed_layer")

    with tf.name_scope("user_fc"):
        # for the first fc
        uid_fc_layer=tf.layers.dense(uid_elayer, config['embed_dim'], name="uid_fc_layer", activation=tf.nn.relu)
        gender_fc_layer=tf.layers.dense(gender_elayer, config['embed_dim'], name="gender_fc_layer", activation=tf.nn.relu)
        age_fc_layer=tf.layers.dense(age_elayer, config['embed_dim'], name ="age_fc_layer", activation=tf.nn.relu)
        occupationfc_layer=tf.layers.dense(occupationembed_layer, config['embed_dim'], name="occupationfc_layer", activation=tf.nn.relu)

        # for the second fc
        user_combine_layer=tf.concat([uid_fc_layer, gender_fc_layer, age_fc_layer, occupationfc_layer], 2)
        user_combine_layer=tf.contrib.layers.fully_connected(user_combine_layer, 200, tf.tanh)
        print("the shape of user_combine_layer, ", user_combine_layer.shape)

        user_clayer_flat=tf.reshape(user_combine_layer, [-1, 200])
        print("the shape of user_clayer_flat, ", user_clayer_flat.shape)

    with tf.name_scope("movie_embedding"):
        movie_id_em_layer=tf.Variable(tf.random_uniform([config['movie_cnt'], config['embed_dim']], -1, 1), name="movie_id_em_layer")
        movie_id_elayer=tf.nn.embedding_lookup(movie_id_em_layer, movie_id, name="movie_id_elayer")

    with tf.name_scope("movie_genres_layers"):
        movie_genres_em_layer=tf.Variable(tf.random_uniform([config['movie_genres_cnt'], config['embed_dim']], -1, 1), name="movie_genres_em_layer")
        movie_genres_elayer=tf.nn.embedding_lookup(movie_genres_em_layer, movie_genres, name="movie_genres_elayer")
        movie_genres_elayer=tf.reduce_sum(movie_genres_elayer, axis=1, keep_dims=True)

    with tf.name_scope("movie_embedding"):
        movie_title_em_layer=tf.Variable(tf.random_uniform([config['movie_title_cnt'], config['embed_dim']], -1, 1), name="movie_title_em_layer")
        movie_title_elayer=tf.nn.embedding_lookup(movie_title_em_layer, movie_titles, name="movie_title_elayer")
        movie_title_elayer_expand=tf.expand_dims(movie_title_elayer, -1)

    with tf.name_scope("movie_fc"):
        movie_id_fc_layer=tf.layers.dense(movie_id_elayer, config['embed_dim'], name="movie_id_fc_layer", activation=tf.nn.relu)
        movie_genres_fc_layer=tf.layers.dense(movie_genres_elayer, config['embed_dim'], name="movie_genres_fc_layer", activation=tf.nn.relu)

        movie_combine_layer=tf.concat([movie_id_fc_layer, movie_genres_fc_layer], 2)
        movie_combine_layer=tf.contrib.layers.fully_connected(movie_combine_layer, 200, tf.tanh)
        print("the shape of movie_combine_layer, ", movie_combine_layer.shape)

        movie_clayer_flat=tf.reshape(movie_combine_layer, [-1, 200])
        print("the shape of movie_clayer_flat, ", movie_clayer_flat.shape)
    
    with tf.name_scope("train_pred"):
        train_pred=tf.reduce_sum(user_clayer_flat * movie_clayer_flat, axis=1)
        train_pred=tf.expand_dims(train_pred, axis=1)

    with tf.name_scope("loss"):
        # MSE error
        cost=tf.losses.mean_squared_error(targets, train_pred )
        print(cost.shape)
        loss=tf.reduce_mean(cost)
    
    # optimize the loss
    global_step=tf.Variable(0, name="global_step", trainable=False)
    optimizer=tf.train.AdamOptimizer(lr)
    gradients=optimizer.compute_gradients(loss)
    trainer=optimizer.apply_gradients(gradients, global_step=global_step)

# In[32]:



# In[33]:


import os
from sklearn.model_selection import train_test_split
# Number of Epochs
num_epochs=5
# Batch Size
batch_size=256

# Learning Rate
learning_rate=0.0001

CNN_pkl="cnn%s.pkl" % suffix
movie_matrics_dat="movie_matrics%s.dat" % suffix
user_matrics_dat="users_matrics%s.dat" % suffix
model_save='./model%s' % suffix

get_ipython().run_line_magic('matplotlib', 'inline')
get_ipython().run_line_magic('config', "InlineBackend.figure_format='retina'")
import matplotlib.pyplot as plt
import time
import datetime

# losses={'train':[], 'test':[]}
train_loss_mat=[]
test_loss_mat=[]

def split_batch(X, Y, batch_size):
    for i in range(0, len(X), batch_size):
        j=min(i + batch_size, len(X))  # for the last episode
        yield X[i:j], Y[i:j]

with tf.Session(graph=train_graph) as sess:
    # draw the graph
    _=tf.summary.FileWriter('./graphs', sess.graph)
    sess.run(tf.global_variables_initializer())
    saver=tf.train.Saver()
    for epoch_i in range(num_epochs):
        
        train_X=features
        train_Y=labels
        
        train_batches=split_batch(train_X, train_Y, batch_size)

        for batch_i in range(len(train_X) // batch_size):
            x, y=next(train_batches)

            categories=np.zeros([batch_size, 18])
            for i in range(batch_size):
                categories[i]=x.take(6,1)[i]

            titles=np.zeros([batch_size, sentences_size])
            for i in range(batch_size):
                titles[i]=x.take(5,1)[i]

            step, train_loss, _=sess.run([global_step, loss, trainer], {
                uid: np.reshape(x.take(0,1), [batch_size, 1]),
                user_gender: np.reshape(x.take(2,1), [batch_size, 1]),
                user_age: np.reshape(x.take(3,1), [batch_size, 1]),
                user_occupation: np.reshape(x.take(4,1), [batch_size, 1]),
                movie_id: np.reshape(x.take(1,1), [batch_size, 1]),
                movie_genres: categories,
                movie_titles: titles,
                targets: np.reshape(y, [batch_size, 1]),
                lr: learning_rate})
            train_loss_mat.append(train_loss)
            
            if (epoch_i * (len(train_X) // batch_size) + batch_i) % 20 == 0:
                print('Epoch={} Batch={}/{}, loss={:.3f}'.format(
                    epoch_i,
                    batch_i,
                    (len(train_X) // batch_size),
                    train_loss))
    # Save Model
    saver.save(sess, model_save)  #, global_step=epoch_i
    print('Model Trained and Saved')


# In[16]:


import tensorflow as tf
import os
import pickle

pickle.dump((model_save), open(CNN_pkl, 'wb'))

model_param=pickle.load(open(CNN_pkl, mode='rb'))

plt.plot(train_loss_mat, label='Training loss')
plt.legend()
_=plt.ylim()


# In[17]:


loaded_graph=tf.Graph()  #
movie_matrics=[]
with tf.Session(graph=loaded_graph) as sess:  #
    # Load saved model
    loader=tf.train.import_meta_graph(model_param + '.meta')
    loader.restore(sess, model_param)

    uid=loaded_graph.get_tensor_by_name("uid:0")
    user_gender=loaded_graph.get_tensor_by_name("user_gender:0")
    user_age=loaded_graph.get_tensor_by_name("user_age:0")
    user_occupation=loaded_graph.get_tensor_by_name("user_occupation:0")
    movie_id=loaded_graph.get_tensor_by_name("movie_id:0")
    movie_genres=loaded_graph.get_tensor_by_name("movie_genres:0")
    movie_titles=loaded_graph.get_tensor_by_name("movie_titles:0")
    targets=loaded_graph.get_tensor_by_name("targets:0")
    lr=loaded_graph.get_tensor_by_name("lr:0")
    movie_clayer_flat=loaded_graph.get_tensor_by_name("movie_fc/Reshape:0")

    for item in movies.values:
        categories=np.zeros([1, 18])
        categories[0]=item.take(2)

        titles=np.zeros([1, sentences_size])
        titles[0]=item.take(1)

        movie_clayer_flat_val=sess.run([movie_clayer_flat], {
            movie_id: np.reshape(item.take(0), [1, 1]),
            movie_genres: categories,
            movie_titles: titles,})  
        movie_matrics.append(movie_clayer_flat_val)

pickle.dump((np.array(movie_matrics).reshape(-1, 200)), open(movie_matrics_dat, 'wb'))
movie_matrics=pickle.load(open(movie_matrics_dat, mode='rb'))


loaded_graph=tf.Graph()  #
users_matrics=[]
with tf.Session(graph=loaded_graph) as sess:  #
    # Load saved model
    loader=tf.train.import_meta_graph(model_param + '.meta')
    loader.restore(sess, model_param)

    uid=loaded_graph.get_tensor_by_name("uid:0")
    user_gender=loaded_graph.get_tensor_by_name("user_gender:0")
    user_age=loaded_graph.get_tensor_by_name("user_age:0")
    user_occupation=loaded_graph.get_tensor_by_name("user_occupation:0")
    movie_id=loaded_graph.get_tensor_by_name("movie_id:0")
    movie_genres=loaded_graph.get_tensor_by_name("movie_genres:0")
    movie_titles=loaded_graph.get_tensor_by_name("movie_titles:0")
    targets=loaded_graph.get_tensor_by_name("targets:0")
    lr=loaded_graph.get_tensor_by_name("lr:0")
    user_clayer_flat=loaded_graph.get_tensor_by_name("user_fc/Reshape:0")

    for item in users.values:

        user_clayer_flat_val=sess.run([user_clayer_flat], {
            uid: np.reshape(item.take(0), [1, 1]),
            user_gender: np.reshape(item.take(1), [1, 1]),
            user_age: np.reshape(item.take(2), [1, 1]),
            user_occupation: np.reshape(item.take(3), [1, 1]),})  
        users_matrics.append(user_clayer_flat_val)

pickle.dump((np.array(users_matrics).reshape(-1, 200)), open(user_matrics_dat, 'wb'))
