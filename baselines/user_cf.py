import pandas as pd
import numpy  as np
from sklearn.model_selection import train_test_split

user_id = 1


ratings = pd.read_table('ratings.dat', header=None, usecols = [0, 1, 2], sep='::')
ratings_train, ratings_test = train_test_split(ratings, test_size=0.2, train_size=0.8)
ratings_train = np.array(ratings_train)
ratings_test = np.array(ratings_test)
del ratings
# users = pd.read_table('users.dat', header=None, usecols = [0, 1, 2], sep='::')
# movies = pd.read_table('movies.dat', header=None, usecols = [0, 1, 2], sep='::')
# users_len = users.iloc[-1][0]
# movies_len = movies.iloc[-1][0]
# del users
# del movies
users_len = 6040
movies_len = 3952


def cos_sim(x, y):
    point_mul = np.matmul(x, y.T)
    self_mul = np.sqrt(np.matmul(x, x.T)) * np.sqrt(np.matmul(y, y.T))
    return (point_mul / self_mul)


user_movie = np.zeros((users_len, movies_len))
for i in range(len(ratings_train)):
    user_movie[ratings_train[i][0]-1][ratings_train[i][1]-1] = ratings_train[i][2]
user_sim_mat = np.zeros((users_len, users_len))
for i in range(1, len(user_sim_mat)):
    for j in range(0, i):
        user_sim_mat[i][j] = cos_sim(user_movie[i], user_movie[j])
        user_sim_mat[j][i] = user_sim_mat[i][j]

selected_user_movie_list = user_movie[user_id - 1]
selected_user_movie_new = []

for i in range(len(selected_user_movie_list)):
    if selected_user_movie_list[i] == 0:
        selected_user_movie_new.append(i)
recommend_list = {}
for i in selected_user_movie_new:
    for j in range(users_len):
        if user_movie[j][i] != 0:
            if i not in recommend_list:
                recommend_list[i] = user_sim_mat[user_id - 1][j] * user_movie[j][i]
            else:
                recommend_list[i] += user_sim_mat[user_id-1][j] * user_movie[j][i]
sort_recommended_list = sorted(recommend_list.items(), key=lambda dic_dat: dic_dat[1], reverse=True)

