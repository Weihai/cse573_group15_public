import numpy as np
import pandas as pd
import pdb
import torch
import torch.optim as optim
from torch.autograd import Variable
from sklearn.model_selection import KFold

movies_100k = pd.read_csv('ml-100k/u.item',header=None, sep = '|', encoding = 'utf-8')
# 1682*24  movie id | movie title | release date | video release date |IMDb URL | 
        # unknown | Action | Adventure | Animation |
        # Children's | Comedy | Crime | Documentary | Drama | Fantasy |
        # Film-Noir | Horror | Musical | Mystery | Romance | Sci-Fi |
        # Thriller | War | Western |
        # The last 19 fields are the genres (1 and 0)

ratings_100k = pd.read_csv('ml-100k/u.data',header=None, delimiter = '\t')
# (100,000*4 user id | item id | rating | timestamp.

users_100k = pd.read_csv('ml-100k/u.user', header=None, delimiter = '|')
# 943*5  user id | age | gender | occupation | zip code


fold = 5#1,2,3,4,5

# m1-100k the 1st fold: u1.base for training, u1.test for testing
# m1-100k the 2nd fold: u2.base for training, u2.test for testing
training_set = pd.read_csv('ml-100k/u'+str(fold)+'.base', header=None, delimiter = '\t')
# size m*4, UserID::MovieID::Rating::Timestamp
training_set = np.array(training_set, dtype = 'int')
test_set = pd.read_csv('ml-100k/u'+str(fold)+'.test', header=None, delimiter = '\t')
# size m*4, UserID::MovieID::Rating::Timestamp
test_set = np.array(test_set, dtype = 'int')

def data_preprocessing(data):
    feature = []
    target = []

    # the user's id is start from 1
    for rating_index in range(data.shape[0]):
        if rating_index%1000==0:
            print(rating_index)
        current_user_id = data[rating_index][0]
        current_movie_id = data[rating_index][1]
        current_movie_type = movies_100k.iloc[current_movie_id-1][5:23].values
        current_movie_rating = data[rating_index][2]

        gender = users_100k.iloc[current_user_id-1][2]
        if gender =='F':
            current_user_gender = 0
        else:
            current_user_gender = 1

        age = users_100k.iloc[current_user_id-1][1]
        if age<18:
            current_user_age = 0
        elif age<25:
            current_user_age = 1
        elif age<35:
            current_user_age = 2
        elif age<45:
            current_user_age = 3
        elif age<50:
            current_user_age = 4
        elif age<56:
            current_user_age = 5
        else:
            current_user_age = 6

        occupation = users_100k.iloc[current_user_id-1][3]
        if occupation == "doctor":
            current_user_occupation = 20
        elif occupation ==  "salesman":
            current_user_occupation = 19
        elif occupation ==  "retired":
            current_user_occupation = 18
        elif occupation ==  "healthcare":
            current_user_occupation = 17
        elif occupation ==  "none":
            current_user_occupation = 16
        elif occupation ==  "marketing":
            current_user_occupation = 15
        elif occupation ==  "engineer":
            current_user_occupation = 14
        elif occupation ==  "artist":
            current_user_occupation = 13
        elif occupation ==  "homemaker":
            current_user_occupation = 12
        elif occupation ==  "librarian":
            current_user_occupation = 11
        elif occupation ==  "programmer":
            current_user_occupation = 10
        elif occupation ==  "entertainment":
            current_user_occupation = 9
        elif occupation ==  "scientist":
            current_user_occupation = 8
        elif occupation ==  "educator":
            current_user_occupation = 7
        elif occupation ==  "lawyer":
            current_user_occupation = 6
        elif occupation ==  "student":
            current_user_occupation = 5
        elif occupation ==  "administrator":
            current_user_occupation = 4
        elif occupation ==  "executive":
            current_user_occupation = 3
        elif occupation ==  "writer":
            current_user_occupation = 2
        elif occupation ==  "technician":
            current_user_occupation = 1
        else:
            current_user_occupation = 0
        # print(current_user_occupation)

        user_feature=np.array([current_user_gender,current_user_age,current_user_occupation])
        movie_feature = np.asarray(current_movie_type,dtype=int)
        feature.append(list(np.concatenate((user_feature,movie_feature),axis=0)))
        target.append(list(np.array([current_movie_rating])))
    return feature,target

train_feature,train_target = data_preprocessing(training_set)
test_feature,test_target = data_preprocessing(test_set)
with open("./processed/train_feature"+str(fold)+".txt", "w") as file:
    file.write(str(train_feature))
with open("./processed/train_target"+str(fold)+".txt", "w") as file:
    file.write(str(train_target))
with open("./processed/test_feature"+str(fold)+".txt", "w") as file:
    file.write(str(test_feature))
with open("./processed/test_target"+str(fold)+".txt", "w") as file:
    file.write(str(test_target))
