# cse573_group15_public

the project repository for the Web Semantic Mining
![](https://gitlab.com/Weihai/cse573_group15_public/-/raw/master/4-26-2020-grhgv.gif)

Slides: https://docs.google.com/presentation/d/1uoa2RDzC8TYW9RHwaJgut1sGeBDC9RaBoA62bwdi224/edit#slide=id.p3

## How to run this code
```
# start the service
cd ./cse573_group15_public
python app.py

# open the index.html located in ./templates/index.html

```

## users matrix and movie matrix
```
cd ./cse573_group15_public 
tensorboard --logdir ./graphs
```