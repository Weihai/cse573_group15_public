import tensorflow as tf
import pickle
import os
import numpy as np
import random
from os.path import abspath, dirname, sys

BASE_PATH = abspath(dirname(abspath(__file__)))
CNN_pkl = "cnn.pkl"
movie_matrics = "movie_matrics.dat"
user_matrics = "users_matrics.dat"

CNN_pkl_without_title = "cnn_without_title.pkl"
movie_matrics_without_title = "movie_matrics_without_title.dat"
user_matrics_without_title = "users_matrics_without_title.dat"

model_param = pickle.load(open(CNN_pkl, mode="rb"))
model_param_without_title = pickle.load(open(CNN_pkl_without_title, mode="rb"))
movies = []
with open(os.path.join(BASE_PATH, "ml-1m/movies.dat"), 'r', encoding="ISO-8859-1") as f:
    for i, line in enumerate(f):
        line = line.strip('\r\n')
        movieid, title = line.split('::')[:2]
        movies.append((movieid, title))

movie_id_idx_mapping = {v[0]:j for j, v in enumerate(movies)}
movie_matrics = pickle.load(open(movie_matrics, mode='rb'))
users_matrics = pickle.load(open(user_matrics, mode='rb'))
movie_matrics_without_title = pickle.load(open(movie_matrics_without_title, mode='rb'))
users_matrics_without_title = pickle.load(open(user_matrics_without_title, mode='rb'))

# ("id", "Title")
def get_rec_by_user_id(user_id, without_title=0):
    # print("without_title: ", without_title)

    loaded_graph = tf.Graph()  #
    with tf.Session(graph=loaded_graph) as sess:
        if without_title == 0:
            loader = tf.train.import_meta_graph(model_param + '.meta')
            loader.restore(sess, model_param)
            embeddings = (users_matrics[user_id - 1]).reshape([1, 200])
            sim = tf.matmul(embeddings, tf.transpose(movie_matrics))
            sim = sim.eval()
            sim = np.squeeze(sim)
            movieIdxs = np.argsort(sim)[-100:]
        else:
            loader = tf.train.import_meta_graph(model_param_without_title + '.meta')
            loader.restore(sess, model_param_without_title )
            embeddings = (users_matrics_without_title[user_id - 1]).reshape([1, 200])
            sim = tf.matmul(embeddings, tf.transpose(movie_matrics_without_title))
            sim = sim.eval()
            sim = np.squeeze(sim)
            movieIdxs = np.argsort(sim)[-100:]

        ans = set()
        for i in range(100):
            if len(ans) >= 10:
                break
            ans.add(movies[movieIdxs[i]])
        return list(ans)


# ("id", "Title")
def get_rec_by_movie_id(movie_id, without_title=0):
    # print("without_title: ", without_title)

    graph = tf.Graph()
    with tf.Session(graph=graph) as sess:
        print("without_title is: ", without_title)
        if without_title == 0:
            loader = tf.train.import_meta_graph(model_param + '.meta')
            loader.restore(sess, model_param)
            movie_id = str(movie_id)
            p_movie_embeddings = (movie_matrics[movie_id_idx_mapping[movie_id]]).reshape([1, 200])
            p_f_sim = tf.matmul(p_movie_embeddings, tf.transpose(users_matrics))
            favorite_user_id = np.argsort(p_f_sim.eval())[0][-100:]
        
            print("The movie is %s" % (movies[movie_id_idx_mapping[movie_id]], ))
            
            p_embeddings = (users_matrics[favorite_user_id-1]).reshape([-1, 200])
            p_sim = tf.matmul(p_embeddings, tf.transpose(movie_matrics))
            sim = p_sim.eval()
        else:
            loader = tf.train.import_meta_graph(model_param_without_title + '.meta')
            loader.restore(sess, model_param_without_title )
            movie_id = str(movie_id)
            p_movie_embeddings = (movie_matrics_without_title[movie_id_idx_mapping[movie_id]]).reshape([1, 200])
            p_f_sim = tf.matmul(p_movie_embeddings, tf.transpose(users_matrics_without_title))
            favorite_user_id = np.argsort(p_f_sim.eval())[0][-100:]
        
            # print("The movie is %s" % (movies[movie_id_idx_mapping[movie_id]], ))
            
            p_embeddings = (users_matrics_without_title[favorite_user_id-1]).reshape([-1, 200])
            p_sim = tf.matmul(p_embeddings, tf.transpose(movie_matrics_without_title))
            sim = p_sim.eval()

        ans = set()
        for val in np.argmax(sim, 1):
            if len(ans) >= 10:
                break

            ans.add(movies[val])
        
        return list(ans)

if __name__ == "__main__":
    print("recomend movie by movie id:")
    print(get_rec_by_movie_id(1401))
    print("recomend movie by user id:")
    print(get_rec_by_user_id(110))
